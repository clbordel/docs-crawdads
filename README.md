# Sources

## Mail server setup

https://medium.com/@stefanledin/setup-the-php-mail-function-on-a-ubuntu-server-c98180e22d82

https://serverfault.com/questions/59602/where-to-check-log-of-sendmail

## Apache 2 setup on Ubuntu

https://ubuntu.com/tutorials/install-and-configure-apache#4-setting-up-the-virtualhost-configuration-file

https://stackoverflow.com/questions/53358023/how-can-i-run-an-index-html-file-on-my-localhost-server

## Important file locations

### Logs 

/var/log/mail.log

### Webfiles

/var/www/docs

ls /etc/apache2/sites-available/
000-default.conf  default-ssl.conf  docs.conf

### Postfix configs

https://superuser.com/questions/280165/how-do-i-change-postfix-port-from-25-to-587